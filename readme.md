# Symfony

**System (Debian-Based) **
    $ sudo apt-get install php5-intl php5-sqlite
    $ sudo php5-enmod pdo_sqlite

**Setup**

    $ composer create-project symfony/framework-standard-edition intergalatic/ ~2.5
    $ cd intergalatic
    $ php app/check.php

## Cliff Notes

The `config/parameters.yaml` is .gitignored by default, create separately.

Remove `incenteev` from `composer.json` after initial SQLite setup.
Incenteev breaks the config, as in removes params when updating.

    "incenteev/composer-parameter-handler": "~2.0" 
    
And two other sections under `post-update` & `post-install`:

    "Incenteev\\"*

**Running the Test Server**

    $ php app/console server:run

**Setting up SQLite3**
(Context: app/config/parameters.yaml)

    (!) database_path: %kernel.root_dir%/../data/%database_name%
    (!) Make sure the ../ is there to get into the data folder

(Context: app/config/config.yaml)

    (!) Uncomment doctrine:dbal:path

**Lingo**
- Entity is really a Business Model
- Validators are called Constraints
- Entity underscored table names are lowerCamelCased in views
  - Same with queryBuilder

** After Creating an Entity: **

    $ php app/console doctrine:schema:update --force


## Basic Outline

#### 1: Create App
- [X] Use SQLITE
- [X] Bundle: WallPostBundle
- [X] Entity: WallPost

#### 2: Form
- [X] Autoset: createDate
- [X] title  (Label = Title)
- [X] body  (Label = Message , Type = textarea)


#### 3: WallPost Controller
- [X] indexAction: list the WallPosts Descending by createDate
- (!) Render the WallPost form below the list of WallPosts and have it submit to the createAction
    - Having a lot of trouble on to make it display errors on the same page
- [X] createAction to process the WallPostForm and insert the new WallPost Record into DB
  - [X] Then redirects to the indexAction


#### 4: Extra
- [X] 1. Create a Bitbucket repo
    - [X] Have a URL test project
- [X] 2. Title length 10/40 characters long
- [X] 3. Paginate the WallPost List Page.
