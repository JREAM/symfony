<?php

namespace JREAM\WallPostBundle\Entity;

use Doctrine\ORM\Mapping as ORM,
    Symfony\Component\Validator\Mapping\ClassMetadata,
    Symfony\Component\Validator\Constraints as Assert;

/**
 * Post
 */
class Post
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $body;

    /**
     * @var \DateTime
     */
    private $createDate;

    /**
     * Used for validating
     *
     * @param  ClassMetadata $metadata
     * @return void
     */
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        // This would have been better with YAML in retrospect
        $metadata->addPropertyConstraint('title', new Assert\Length([
            'min'        => 10,
            'max'        => 40,
            'minMessage' => 'Title has to be atleast {{ limit }} characters',
            'maxMessage' => 'Title cannot exceed {{ limit }} characters'
        ]));

        $metadata->addPropertyConstraint('body', new Assert\Length([
            'min'        => 1,
            'max'        => 2000,
            'minMessage' => 'Body has to be atleast {{ limit }} characters',
            'maxMessage' => 'Body cannot exceed {{ limit }}'
        ]));
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Post
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set body
     *
     * @param string $body
     * @return Post
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set createDate
     *
     * @param \DateTime $createDate
     * @return Post
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;

        return $this;
    }

    /**
     * Get createDate
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }
}
