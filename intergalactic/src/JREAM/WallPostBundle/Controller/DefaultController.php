<?php

namespace JREAM\WallPostBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller,
    JREAM\WallPostBundle\Entity\Post,
    Symfony\Component\HttpFoundation\Request,
    Symfony\Component\HttpFoundation\Response,
    Symfony\Component\HttpFoundation\RedirectResponse,

    // For the errors from the form
    Symfony\Component\HttpFoundation\Session\Session;

    // For sorting, nah, ill use the query builder.
    //Doctrine\Common\Collections\Criteria;


class DefaultController extends Controller
{

    public $session;

    // --------------------------------------------------------------

    /**
     * Displays as the main site landing page
     *
     * @return object (I think)
     */
    public function indexAction(/**$override_form = false <-- An attempt for error messages, cancelled*/)
    {
        // Not sure if this should go here, but Im just trying to pass error messages
        // from the form validator to another view.
        // $this->session = new Session();
        // $this->session->invalidate();
        // $this->session->start();
        //
        // ** Ill use Forwarding Response instead of Session.
        // So I need to seaparate my Query fetching into a private method

        return $this->render('WallPostBundle:Default:index.html.twig', [
            'pagination' => $this->_getPaginatedPosts(),
            // Below: An attempt for displaying error messages from the form
            // 'form' => ($override_form) ? $override_form : $this->_generateForm()->createView()
            'form' => $this->_generateForm()->createView()
        ]);
    }

    // --------------------------------------------------------------

    /**
     * Creates the Record
     *
     * @param  Request $request (Handled by the Form)
     *
     * @return mixed
     */
    public function createAction(Request $request)
    {
        $form = $this->_generateForm();

        // Handles Submissions (hence the $request arguement)
        $form->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($form);

        if ($form->isValid()) {

            // Translate form into data object
            $entity = $this->getDoctrine()->getManager();
            $entity->persist($form->getData());
            $entity->flush();

            return new RedirectResponse($this->generateUrl('wall_post_homepage'));
        }


        // Can't seem to get this to pass errors
        // ---
        // return $this->forward('WallPostBundle:Default:index', [
        //     'form' => $this->_generateForm()->createView()
        // ]);
        return $this->render('WallPostBundle:Default:create.html.twig', [
            'form' => $form->createView()
        ]);

    }

    // --------------------------------------------------------------

    private function _getPaginatedPosts()
    {
        $posts = $this->getDoctrine()->getRepository('WallPostBundle:Post');
        // $posts->findAll(); <-- can't use because you can't attach an orderBy, yet
        // you can with findBy()? I hope I'm wrong.

        $query = $posts->createQueryBuilder('p')
            ->orderBy('p.createDate', 'DESC')
            ->getQuery();
        // $posts = $query->getResult();

        $paginator = $this->get('knp_paginator');
        return $paginator->paginate(
            $query,
            $this->get('request')->query->get('page', 1),
            5 // Per Page
        );

    }

    // --------------------------------------------------------------

    private function _generateForm()
    {
        // So This prepares an empty post guy, I wanna leave the fields empty
        $post = new Post();
        $post->setCreateDate(new \Datetime); // I'd like this to be in the (Entity, don't see where yet)

        // Generates DOM stuff
        $form = $this->createFormBuilder($post)
            ->add('title', 'text')
            ->add('body', 'textarea')
            ->add('save', 'submit')
            ->setAction($this->generateUrl('wall_post_create'))
            ->getForm();

        return $form;
    }

    // --------------------------------------------------------------

}
// --------------------------------------------------------------
// End of File